#!/bin/bash
###############################################################################
################## Realizar o add partitions tabela External ##################
###############################################################################
#
### D - informar Database que receberá os dados
D=$1
#
### T - informar Tabela de origem
T=$2
#
if [[ -z ${D} || -z ${T} ]]; then
echo ''
echo ''
echo 'Parametros nao preenchidos corretamente! Interrompendo execucao...'
echo ''
echo 'Modo de uso:'
echo '    '${0}' [Database que receberá os dados] [Tabela]'
echo ''
echo '                  Exemplo: '${0}' ' 'XY XYZ002 '
echo ''
exit 1
fi
#
###
#
if [ -f  "/sistema/bdf/SANITIZACAO/SELECT_HIVE/addpartitions_$T.hql" ] ; then
        echo "addpartitions_$T.hql ja existe arquivo."
        echo " "
        echo "apagando arquivo e recriando"
        rm /sistema/bdf/SANITIZACAO/SELECT_HIVE/addpartitions_$T.hql
        echo " "
        echo "Iniciando Processo..."
        echo " "
else
        echo " "
        echo "Iniciando Processo..."
fi
#
echo "   *** Adicionando Partições ***"
echo "*** TABELA "$2" DATABASE "$1" ***"
echo " "
# LISTA DE ODATES
ODATES=`hive -e "show partitions SANITIZACAO.$T;" | awk -F'=' '{print $2}'`
echo " "
#
#
for PARTICOES in $ODATES
do
#
#  Formata variaveis de data
   DIA=$(echo $PARTICOES | cut -c9-10)
   MES=$(echo $PARTICOES | cut -c6-7)
   ANO=$(echo $PARTICOES | cut -c3-4)
   DDMMAA=$DIA$MES$ANO
#
   echo "Adicionando Partição: $T: " $PARTICOES " - " $DDMMAA
#
#     Realiza procedimentos:
#     Adiocionar os parquet a tabela.
#
                echo "ALTER TABLE $D.$T add if not exists partition (dat_ref_carga='$PARTICOES') location '/sistemas/bdi/$D/$T/parquet/odate=$DDMMAA';" >> /sistema/bdf/SANITIZACAO/SELECT_HIVE/addpartitions_$T.hql
#
echo "Finalizado o parquet dat_ref_carga=" $PARTICOES " - location /sistemas/bdi/$D/$T/parquet/odate="$DDMMAA
echo " "
echo " "
echo " "
#
done
#
hive -f /sistema/bdf/SANITIZACAO/SELECT_HIVE/addpartitions_$T.hql
#
echo " "
echo "Processo Finalizado"