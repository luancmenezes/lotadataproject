#importing python libs

import sys
from re import sub, search
from sys import argv
import os
import subprocess
import re
import glob
from os import system
from decimal import Decimal
from datetime import date,datetime, time, timedelta
import logging
import argparse
import codecs
import glob
import re


#loading parse args

parser = argparse.ArgumentParser(prog='AUTO SANITIZACAO', usage='%(prog)s [option1, option2, option3]', description='Realiza a construcao dos confs/defs/hqls conforme a lista de dbs/tbls!')
parser.add_argument('--arquivoLista', type=str, metavar='/sistemas/arquivos/exemplo.txt',
                   help='um arquivo para carregar a lista de databases.tabelas para ingestao - BDI-OLD')

args = parser.parse_args()



def expurgo_hk(val1):
  arq_lista_del_txt = val1
#  arq_list_mkdir = val2
  count_line = 0
  listOfLines = list()
  with open (arq_lista_del_txt, "r") as myfile:
    for line in myfile:
      listConf = line.split('.')
      database = (listConf[0])
      tabela = (listConf[1])
      input_create = "/sistemas/bdi/" + database + "/" + tabela + "/INPUT/"
      input_create = input_create.split('\n')
      input_create = ''.join(input_create)
      parquet_create = "/sistemas/bdi/" + database + "/" + tabela + "/parquet/"
      parquet_create = parquet_create.split('\n')
      parquet_create = ''.join(parquet_create)
      cfg_create = "/sistemas/bdf/" + database + "/cfg/"
      cfg_create = cfg_create.split('\n')
      cfg_create = ''.join(cfg_create)
      listOfLines.extend((input_create, parquet_create, cfg_create))
      fileName = tabela + '.conf'
      fileName = fileName.split('\n')
      fileName = ''.join(fileName)
      with codecs.open (fileName, 'w', encoding='utf8') as conf:
        conf.write("retention=360\n")
        conf.write("hdfs_path_source=" + input_create + '\n')
        conf.write("hdfs_path_save=" + parquet_create + '\n')
        conf.write("os_path_source=/produtos/bdr/mf/" + '\n')
        conf.write("file_count=1\n")
        conf.write("database=" + database + '\n')
        conf.write('quote="\n')
        conf.write("demiliter=;\n")
        conf.write("header=false")
  print(listOfLines)

#expurgo_hk(args.arquivoLista)


def loadSchema(path):
    try:
        coluns = []
        file = open(name, "r")
#        file = sc.textFile(path)
        for lines in file:
#        lines = file.line
          for i in range(0, len(lines)):
              col = lines[i].split(",")
              stype = ''
            #print(lines[i])
              if str(col[1]).startswith('DECIMAL'):
                  stype=col[1]+','+col[2].replace(')','')+')'
              else:
                  stype = col[1]
              coluns.append(StructField(col[0], deParaType(stype), True))
          return coluns
    except  ValueError as ve:
          setError('ERRO: Erro ao montar schema{}'.format(ve))


#for name in glob.glob('*.CTR'):
#    loadSchema(name)


for name in glob.glob('*.CTR'):
    file = open(name, "r")
    for lines in file:
#      col = lines.split(",").split('\n')
      col = re.split('\n|[,]|CHARACTER', lines.replace('NOT NULL', '').replace('CREATE TABLE', '').replace('(', ' ').replace(')', ''))
      col = ''.join(col)
      print col
      #print col
#      for i in range(0, len(col)):
#        new_col = col[i].split(" ")
#        print new_col
#      print(col)
#      listConf = lines.split('\n')
#      listConf = listConf.replace('NOT NULL', '')
#      listConf = listConf.replace('CREATE TABLE', '')
#      listConf = ''.join(listConf)
#      print listConf


#re.split('(\W+)', 'Words, words, words.')
