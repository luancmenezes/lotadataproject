package com.vogella.maven;

public class TypeCrime {
	
	private String ano;
	private String month;
	private String qtd;
	private String type_crime;
	
	public TypeCrime(String ano,String month,String qtd,String type_crime) {
		this.ano = ano;
		this.month = month;
		this.qtd = qtd;
		this.type_crime = type_crime;
	}
	public String getAno() {
		return ano;
	}
	public void setAno(String ano) {
		this.ano = ano;
	}
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
	public String getQtd() {
		return qtd;
	}
	public void setQtd(String qtd) {
		this.qtd = qtd;
	}
	public String getType_crime() {
		return type_crime;
	}
	public void setType_crime(String type_crime) {
		this.type_crime = type_crime;
	}
	
	
	
	
}
