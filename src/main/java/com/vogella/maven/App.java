package com.vogella.maven;

import com.vogella.maven.*;
import java.io.IOException;
import java.time.Month;
import java.util.*;
import java.util.stream.*;
import tech.tablesaw.api.*;
import tech.tablesaw.columns.Column;
import tech.tablesaw.io.csv.CsvReadOptions;
import tech.tablesaw.io.csv.CsvReadOptions.Builder;
import tech.tablesaw.io.csv.CsvReader;
import java.io.FileWriter;
import java.util.Scanner;

/**Object
 * Hello world!
 *
 */
public class App 
{
	private static ColumnType StringColumn;
	private static ColumnType DateColumn;
    public static void main( String[] args ) throws IOException
    {

    	//DECLARE VARIABLES
    	Lotadata table = new Lotadata();
    	ArrayList<String> crime_type = new ArrayList<String>();
    	ArrayList<String> month_ = new ArrayList<String>();
    	ArrayList<String> type_crimes = new ArrayList<String>();
    	ArrayList<TypeCrime> type = new ArrayList<TypeCrime>();
    	
    	//READ CSV
    	String csvFile = "/home/luancmenezes/eclipse-workspace/quickstart/database/f.csv";
        FileWriter writer = new FileWriter(csvFile);
    	Builder builder = CsvReadOptions.builder("/home/luancmenezes/eclipse-workspace/quickstart/database/crimeinfo20181219_.csv")
    			.header(true)
    			.dateFormat("yyyy-MM-dd");
    	CsvReadOptions options = builder.build();
    	
    	Scanner scan = new Scanner(System.in);
    	System.out.println("Write a year to analyse (2014 - 2018) ... ");
        int YEAR_ = scan.nextInt();
        
    	Table t = Table.read().csv(options);
    	//TYPE CRIMES
    	for(int x = 0; x < t.column("ORIG_CRIMETYPE_NAME").summary().column("Category").size();x++) {
    		type_crimes.add(t.column("ORIG_CRIMETYPE_NAME").summary().column("Category").getString(x));
    	}
    	//CREATING OBJECT CRIME
    	for(int year = YEAR_;year < YEAR_+1;year++) {
    		for (int month = 1; month < 13;month ++) {
    			System.out.println("Creating... "+year);
	    		Table tb_year = table.getTableFilteredYear(year,"REPORT_DATE",t);
	        	Table tb_year_month = table.createColumnMonth("REPORT_DATE", tb_year);
	        	Integer qtd =  table.filterMonth(month, tb_year_month).column("ORIG_CRIMETYPE_NAME").summary().column("Category").size();
	        	Column t_filted = table.filterMonth(2, tb_year_month).column("ORIG_CRIMETYPE_NAME").summary().column("Category");
	        	for(int i = 0; i < qtd; i++) {
		    		String type_qtd = table.filterMonth(month, tb_year_month).column("ORIG_CRIMETYPE_NAME").summary().getString(i, 1);
		    		String type_name = table.filterMonth(month, tb_year_month).column("ORIG_CRIMETYPE_NAME").summary().getString(i, 0); 
//		    		System.out.println(table.Month(month));
		    		type.add(new TypeCrime(Integer.toString(year),table.Month(month),type_qtd,type_name));
		    	}
    		}
    		System.out.println("Created. "+year);
    	}
    	
    	
    	
    	//Writing File
    	//for header
        CsvUtils.writeLine(writer, Arrays.asList(Integer.toString(YEAR_),"MONTH","QTD"));
        ArrayList<String> list = new ArrayList<String>();
		for(TypeCrime type_: type) {
			for(int x = 0; x < type_crimes.size();x++) {
				String crime = type_crimes.get(x);
				for (int month = 1; month < 13;month ++) {
					String month_str = table.Month(month);
					if(type_.getType_crime().contains(crime)) {
						if(type_.getMonth().contains(month_str) & type_.getAno().contains(Integer.toString(YEAR_))) {
							list.add(type_.getType_crime());
							list.add(month_str);
							list.add(type_.getQtd());
							CsvUtils.writeLine(writer, list);
							list.clear();
				    		System.out.println("Writing file... ");
						}
					}
				}
			}
		}
		 writer.flush();
	     writer.close();
 		System.out.println("Written. ");
    }
}

