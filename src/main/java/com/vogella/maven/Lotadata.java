package com.vogella.maven;


import java.util.stream.*;
import tech.tablesaw.api.*;
import tech.tablesaw.selection.Selection;
import tech.tablesaw.*;

public class Lotadata {
	public Table getTableFilteredYear(Integer ano,String column,Table t) {	
		DateColumn tb = t.dateColumn(column);
		return t.where(tb.isInYear(ano));
	}
	public Table createColumnMonth(String column, Table t) {
		StringColumn month = t.dateColumn(column).month();
		t.addColumns(month);
		return t;		
	}
	public Table filterMonth(Integer month,Table t) {
		switch(month) {
			case 1:
				return t.where((t.stringColumn("REPORT_DATE month")).isEqualTo("JANUARY"));
			case 2:
				return t.where((t.stringColumn("REPORT_DATE month")).isEqualTo("FEBRUARY"));
			case 3:
				return t.where((t.stringColumn("REPORT_DATE month")).isEqualTo("MARCH"));
			case 4:
				return t.where((t.stringColumn("REPORT_DATE month")).isEqualTo("APRIL"));
			case 5:
				return t.where((t.stringColumn("REPORT_DATE month")).isEqualTo("MAY"));
			case 6:
				return t.where((t.stringColumn("REPORT_DATE month")).isEqualTo("JUNE"));
			case 7:
				return t.where((t.stringColumn("REPORT_DATE month")).isEqualTo("JULY"));
			case 8:
				return t.where((t.stringColumn("REPORT_DATE month")).isEqualTo("AUGUST"));
			case 9:
				return t.where((t.stringColumn("REPORT_DATE month")).isEqualTo("SEPTEMBER"));
			case 10:
				return t.where((t.stringColumn("REPORT_DATE month")).isEqualTo("OCTOBER"));
			case 11:
				return t.where((t.stringColumn("REPORT_DATE month")).isEqualTo("NOVEMBER"));
			case 12:
				return t.where((t.stringColumn("REPORT_DATE month")).isEqualTo("DECEMBER"));
			default:
				return t.where((t.stringColumn("REPORT_DATE month")).isEqualTo("JANUARY"));
		}
	
	}
	public String Month(Integer month) {
		switch(month) {
			case 1:
				return "JAN";
			case 2:
				return "FEB";
			case 3:
				return "MAR";
			case 4:
				return "APR";
			case 5:
				return "MAY";
			case 6:
				return "JUN";
			case 7:
				return "JUL";
			case 8:
				return "AUG";
			case 9:
				return "SEP";
			case 10:
				return "OCT";
			case 11:
				return "NOV";
			case 12:
				return "DEC";
			default:
				return "-";
		}
	}
}
