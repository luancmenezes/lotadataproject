from os import path,system
from pyspark.sql import SparkSession
from pyspark.sql import Row
from pyspark.sql.types import StringType
from pyspark.sql.functions import udf
import pyspark.sql.functions as F
import subprocess
import argparse


parser = argparse.ArgumentParser()
parser.add_argument("--D", help="Database.")
parser.add_argument("--T", help="External Table.")
parser.add_argument("--D_ORIG", help="Database Original.")

args = parser.parse_args()
D = args.D
T = args.T
D_ORIG = args.D_ORIG

class Migrator:
    def __init__(self, db, tb, db_orig):
        self.db = db
        self.tb = tb
        self.db_orig = db_orig
        self.partition = 'dat_ref_carga'
        self.pathHdfs = '/sistemas/bdi/SANITIZACAO/'
    def formatColumn(self,orig):
        orig = orig.partition
        orig = orig.replace('dat_ref_carga=', '')
        year = orig[:4]
        month = orig[5:7]
        day = orig[8:]
        odate = '{0}{1}{2}'.format(day, month, year)
        return orig,odate
    def HiveE(self, part):
        partition , odate = self.formatColumn(part)
        cmd = "ALTER TABLE {0}.{1} partition (dat_ref_carga='{2}') set location '/sistemas/bdi/{0}/{1}/parquet/odate={3}';".format(m.db.upper(),m.tb.upper(),partition,odate)
        print cmd
        system('hive -e "{0}"'.format(cmd))
    def selectDBTable(self):
        query = 'select * from {0}.{1}'.format(self.db_orig,self.tb)
        return query
    def listPathsHdfs(self):
        path_hdfs = "/sistemas/bdi/{0}/{1}/parquet/".format(self.db.upper(),self.tb.upper())
        args = "hdfs dfs -ls " + path_hdfs + " | awk '{print $8}'"
        proc = subprocess.Popen(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
        s_output, s_err = proc.communicate()
        all_dart_dirs = s_output.split()  # stores list of files and sub-directories in 'path'
        return all_dart_dirs
    def renameNamePaths(self,path):
        if 'SUCESS' not in path:
            recover = '/'.join(path.split('/')[:-1])
            name = path.split('/')[-1]
            name = name.replace('dat_ref_carga=','')
            date = self.formatColumn(name)
            n_name = '{0}/odate={1}'.format(recover,date)
            print('hdfs dfs -mv {0} {1}'.format(path,n_name))



m = Migrator(D,T,D_ORIG)
udf_formatColumn = udf(m.formatColumn,StringType())

spark = SparkSession \
    .builder \
    .appName("Python Spark SQL Hive integration example") \
    .config("spark.sql.parquet.compression.codec", "snappy") \
    .enableHiveSupport() \
    .getOrCreate()

## LOAD TABLE
df = spark.sql(m.selectDBTable())

## MIGRATOR
df.write\
.option('path', m.pathHdfs + '{0}/parquet'.format(m.tb))\
.format('parquet')\
.mode('append')\
.partitionBy(m.partition)\
.saveAsTable("{0}.{1}".format(m.db,m.tb))


##ADD PARTITIONS
partitions = spark.sql('show partitions {0}.{1}'.format(m.db,m.tb)).collect()
#pRDD = sc.parallelize(partitions)
#pRDD.map(m.HiveE).count()
[m.HiveE(p) for p in partitions]