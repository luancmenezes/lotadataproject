#!/bin/bash
###############################################################################
# Migracao de historico tabela Gerenciável para Tabela External - SANITIZAÇÃO
###############################################################################
#
### D - informar Database que receberá os dados
D=$1
#
### T - informar Tabela de origem
T=$2
#
### D_ORIG - informar Database Origem
D_ORIG=$3
#
if [[ -z ${D} || -z ${T} || -z ${D_ORIG} ]]; then
echo ''
echo ''
echo 'Parametros nao preenchidos corretamente! Interrompendo execucao...'
echo ''
echo 'Modo de uso:'
echo '    '${0}' [Database que receberá os dados] [Tabela] [Database Origem]'
echo ''
echo '                  Exemplo: '${0}' ' 'SANITIZACAO XYZ002 XY '
echo ''
exit 1
fi
#
echo "*** MIGRANDO PARQUETS ***"
echo "*** TABELA "$2" DATABASE "$3" para DATABASE "$1" TABELA "$2" ***"
echo " "
###
#
QUERY_SELECT=/sistema/bdf/SANITIZACAO/SELECT_HIVE/$2'_select.hql'
LISTAR=`cat $QUERY_SELECT`

# LISTA DE ODATES
ODATES=`hive -e "show partitions $D_ORIG.$T;" | awk -F'=' '{print $2}'`
echo $ODATES
#
#
for PARTICOES in $ODATES

do
#
#  Formata variaveis de data
   DIA=$(echo $PARTICOES | cut -c9-10)
   MES=$(echo $PARTICOES | cut -c6-7)
   ANO=$(echo $PARTICOES | cut -c3-4)
   DDMMAA=$DIA$MES$ANO
#
   echo "Migrar histórico de dados $T: " $PARTICOES " - " $DDMMAA
#
#     Realiza procedimentos:
#     Cria particao e diretorio para os parquets
#     Realiza select -> Insert com tratamento dos campos com diferencas de datatypes
#
                hive -e "set parquet.compression=SNAPPY;
                ALTER TABLE $D.$T ADD IF NOT EXISTS PARTITION(DAT_REF_CARGA='$PARTICOES') location '/sistemas/bdi/$D_ORIG/$T/parquet/odate=$DDMMAA';
                                from $D_ORIG.$T INSERT OVERWRITE TABLE $D.$T PARTITION(dat_ref_carga='$PARTICOES')
                                                                                select
                                                                                $LISTAR
                                                                                where dat_ref_carga = '$PARTICOES';"

#
echo "Finalizado o parquet dat_ref_carga=" $PARTICOES " - location /sistemas/bdi/$D_ORIG/$T/parquet/odate="$DDMMAA
echo " "
echo " "
echo " "
#
done