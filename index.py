
    from pyspark.sql import SparkSession
    spark = SparkSession.builder.appName('ES_indexer').getOrCreate()


schema = StructType([\
StructField("Path", StringType(),True),\
StructField("Replication", IntegerType(),True),\
StructField("ModificationTime", TimestampType(),True),\
StructField("AccessTime", TimestampType(),True),\
StructField("PreferredBlockSize", LongType(),True),\
StructField("BlocksCount", IntegerType(),True),\
StructField("FileSize", IntegerType(),True),\
StructField("NSQUOTA", ByteType(),True),\
StructField("DSQUOTA", ByteType(),True),\
StructField("Permission", StringType(),True),\
StructField("UserName", StringType(),True),\
StructField("GroupName", StringType(),True)])
    
df = spark.read.csv("/home/luanmenezes/Downloads/fsimage_BDR-DCN-ANA_pipe_delm.csv",\
    header=True)

df.write.format('org.elasticsearch.spark.sql').\
    option('es.nodes', 'http://172.17.0.2').\
    option('es.port', 9200).\
    option('es.resource', '%s/%s' % ('fsimagefake', 'datalake')).\
    save()

curl -X DELETE "http://172.17.0.2:9200/fsimage?pretty"